//
//  BirdsDetailViewController.h
//  BirdWatching
//
//  Created by Thomas Knauf on 19.01.13.
//  Copyright (c) 2013 MetaObjekt GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BirdsDetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
