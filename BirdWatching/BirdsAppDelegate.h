//
//  BirdsAppDelegate.h
//  BirdWatching
//
//  Created by Thomas Knauf on 19.01.13.
//  Copyright (c) 2013 MetaObjekt GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BirdsAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
