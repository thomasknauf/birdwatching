//
//  main.m
//  BirdWatching
//
//  Created by Thomas Knauf on 19.01.13.
//  Copyright (c) 2013 MetaObjekt GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BirdsAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BirdsAppDelegate class]));
    }
}
